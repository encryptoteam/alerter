package main

import (
	"embed"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/config"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/controller"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/database/model"
	"log"
)

//go:embed tmpl/*.html
var webUI embed.FS

func main() {
	log.SetFlags(log.Lshortfile)
	conf, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	dbConn := model.InitDatabase(conf.SqlLitePathDB)

	controller.InitializeControllerInstances(dbConn)
	//database.CreateTables()
	//database.FillTables()

	internal.StartTracking(conf, dbConn)
	//web.WebUI = webUI
	//web.StartServer(conf.WebServerAddr)
}
