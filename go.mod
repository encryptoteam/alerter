module gitlab.com/encryptoteam/rocket-apps/services/alerter

go 1.16

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/bykovme/goconfig v0.0.0-20170717154220-caa70d3abfca
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/hashicorp/go-version v1.4.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/tidwall/gjson v1.8.1
	github.com/tidwall/pretty v1.2.0 // indirect
	gitlab.com/encryptoteam/rocket-apps/services/proto v0.6.1
	google.golang.org/grpc v1.40.0
)
